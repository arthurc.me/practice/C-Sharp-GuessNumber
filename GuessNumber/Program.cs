﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace GuessNumber
{
    class MainClass
    {
        private static bool DEBUG = false;
        private static string TAG = "GuessNumber";

        public static void Log(string str)
        {
            Log(TAG, str);
        }

        public static void Log(string tag, string str)
        {
            if (!DEBUG)
            {
                return;
            }

            Console.WriteLine(tag + ": " + str);
        }

        public static List<char> GetTopic()
        {
            Random random = new Random();
            List<char> number = new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            List<char> result = number.OrderBy(x => random.Next()).Take(4).ToList();
            Log("topic", String.Join("", result) + "\n");

            if (result[0] == 0)
            {
                result = GetTopic();
            }

            return result;
        }

        public static string GetUserInput()
        {
            string strInput;
            while (true)
            {
                // Get user input.
                Console.Write("Please input a number: ");
                strInput = Console.ReadLine();

                // Check input is number or not.
                bool isNumber = int.TryParse(strInput, out int intInput);
                if (!isNumber)
                {
                    Console.WriteLine("Not number.");
                    continue;
                }

                // Check first number is not 0
                if (strInput[0] == '0')
                {
                    Console.WriteLine("First number can't be 0.");
                    continue;
                }

                // Check input length.
                if (strInput.Length != 4)
                {
                    Console.WriteLine("Length is not 4.");
                    continue;
                }

                // Check input is non-repetitive
                bool flag = false;
                foreach (char c in strInput)
                {
                    int firstChar = strInput.IndexOf(c);
                    if (strInput.IndexOf(c, firstChar + 1) != -1)
                    {
                        Console.WriteLine("Number {0} appear twice or more.", c);
                        flag = true;
                        break;
                    }
                }

                if (flag)
                {
                    continue;
                }

                return strInput;
            }
        }

        public static int[] GetResult(List<char> topic, string userInput)
        {
            int a = 0;
            int b = 0;
            for (int i = 0; i < 4; i++)
            {
                if (topic[i] == userInput[i])
                {
                    a++;
                    continue;
                }

                if (topic.Contains(userInput[i]))
                {
                    b++;
                    continue;
                }
            }

            return new int[] { a, b };
        }

        public static void Main(string[] args)
        {
            List<char> topic = GetTopic();
            int gameCount = 0;

            while (true)
            {
                gameCount++;

                string userInput = GetUserInput();
                int[] result = GetResult(topic, userInput);

                Console.WriteLine("Result: {0}A{1}B.\n", result[0], result[1]);

                if (result[0] == 4)
                {
                    Console.WriteLine("Game finish.");
                    Console.WriteLine("You guessed {0} times.", gameCount);
                    break;
                }
            }
        }
    }
}
